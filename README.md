# Wiki Article Images Downloader

This script downloads all the uncompressed images in a wiki article. Useful if you want all the uncompressed images without going image by image.

## Usage
It goes without saying that you need Python installed for the script to work.

Invoke it with `./waid.py` or `python waid.py`.

To download the images in a wiki article, specify the wiki's api.php url and article like so: `./waid.py -w WIKI-URL ARTICLE`. `WIKI-URL` is the wiki's api.php url, which can be found by going to Special:Version in your wiki. `ARTICLE` is the article's url name.

The script will write images to `./waid_images/` by default. It will ignore images not directly stated in the article. This behaviour can be changed with `--all` (or `-a`) which downloads all the images including those from indirect sources.

### Options
```help
positional arguments:
  ARTICLE                 article title

options:
  -h, --help              show this help message and exit
  -w URL, --wiki URL      wiki's api.php url (example: "https://hollowknight.wiki/mw/api.php"). You can find the api.php url by going to Special:Version in your wiki
  -o PATH, --output PATH  image download path
  -a, --all               download ALL the images
  -f, --force             force writing to fwaid_images/ or the directory specified by --output (WARNING: you may lose data in doing so)
```

## Configuration
It is possible to change the default behaviour by editing the script itself. The variables `WIKI` and `OUTPUT` change their respective options.
