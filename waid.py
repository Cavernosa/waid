#!/usr/bin/python
import requests
from time import sleep
from shutil import copyfileobj
import sys
import os
import argparse

WIKI = 'https://hollowknight.wiki/mw/api.php'
OUTPUT = 'waid_images/'

parser = argparse.ArgumentParser(
		prog="waid.py",
		description="Downloads images in a wiki article",
		formatter_class=lambda prog: argparse.HelpFormatter(prog,max_help_position=28)
		)
parser.add_argument('title', metavar='ARTICLE',
					help='article title')
parser.add_argument('-w', '--wiki', metavar='URL', 
					default=WIKI,
					help='wiki\'s api.php url (example: "https://hollowknight.wiki/mw/api.php"). You can find the api.php url by going to Special:Version in your wiki')
parser.add_argument('-o', '--output', metavar='PATH',
					default=OUTPUT,
					help='image download path')
parser.add_argument('-a', '--all',
					action='store_true',
					help='download ALL the images')
parser.add_argument('-f', '--force',
					action='store_true',
					help='force writing to fwaid_images/ or the directory specified by --output (WARNING: you may lose data in doing so)')
args = parser.parse_args()

# Creating image directory
try:
	os.makedirs(args.output)
except FileExistsError:
	dir_exists = 'directory "' + args.output + '" already exists.'
	if not args.force:
		print('ERROR: directory "', args.output, '" already exists. Please choose a different directory or override this error with --force')
		sys.exit(1)

params = {
		"action": "query",
		"prop": "images",
		"titles": args.title,
		"format": "json"
		}

img_params = {
		"action": "query",
		"prop": "imageinfo",
		"iiprop": "url",
		"format": "json"
		}

wikitext_params = {
		'action': 'parse',
		'page': args.title,
		'prop': 'wikitext',
		'format': 'json'
		}

ENDPOINT = args.wiki

print(ENDPOINT)

def jprint(obj):
	import json
	text = json.dumps(obj, sort_keys=True, indent=4)
	print(text)

# Getting and preparing wikitext for comparing later
wikitext_response = requests.get(ENDPOINT, params=wikitext_params)
wikitext = wikitext_response.json()['parse']['wikitext']['*']
## Parsing {{PAGENAME}} and replacing spaces with underlines
wikitext = wikitext.replace('{{PAGENAME}}', wikitext_response.json()['parse']['title']).replace(' ', '_')


def get_img(img_params, img, filename):
	# Getting full url of the file 
	img_params["titles"] = img['title']
	img_request = requests.get(ENDPOINT, params=img_params)
	img_url = next(iter(img_request.json()['query']['pages'].values()))['imageinfo'][0]['url']
	# Request image for download
	dl_request = requests.get(img_url, params={"format":"original"}, stream=True)
	# Save image locally
	with open(args.output + filename, 'wb') as real_file:
		copyfileobj(dl_request.raw, real_file)
	print("Got", filename)
	sleep(0.5)

while True:
	response = requests.get(ENDPOINT, params=params)
	page_images = next(iter(response.json()['query']['pages'].values()))['images']

	for img in page_images:
		filename = img['title'][img['title'].find(':') + 1:].replace(' ', '_') # File_name.png
		if args.all:
			get_img(img_params, img, filename)
		elif filename in wikitext:
			get_img(img_params, img, filename)

	if "continue" in response.json():
		# Create imcontinue in case there's more images
		params["imcontinue"] = response.json()['continue']['imcontinue']
		sleep(0.5)
		continue
	break



